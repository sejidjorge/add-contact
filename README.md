Este projeto foi inicializado com [Create React App](https://github.com/facebook/create-react-app).

Esta aplicação serve para enviar mensagens no WhatsApp para contatos que não estão salvos inicialmente em seu dispositivo, você também pode gerar um QR-code para seu próprio número.

Você pode clonar este repositorio e modificar/testar a vontade.

Esta aplicação utiliza ReactJS + Material Ui + Material Design Icons

## Scripts Disponíveis

No diretório do projeto, você pode executar:

### `npm start`

Executa o aplicativo no modo de desenvolvimento.\
Abra [http://localhost:3000](http://localhost:3000) para visualizá-lo em seu navegador.

A página será recarregada quando você fizer alterações.\
Você também pode ver erros de lint no console.

### `npm test`

Inicia o executor de teste no modo de exibição interativa.\
Consulte a seção sobre [execução de testes](https://facebook.github.io/create-react-app/docs/running-tests) para obter mais informações.

### `npm run build`

Compila o aplicativo para produção na pasta `build`.\
Ele agrupa corretamente o React no modo de produção e otimiza a compilação para o melhor desempenho.

A compilação é reduzida e os nomes dos arquivos incluem os hashes.\
Seu aplicativo está pronto para ser implantado!

Consulte a seção sobre [implantação](https://facebook.github.io/create-react-app/docs/deployment) para obter mais informações.

